# CI/CD With Jenkins and Docker-Compose

## files

```
docker-compose.export.yml <-- exports volume data
docker-compose.yml <-- jenkins + test DB + jenkins config volume
jenkins.env <-- env vars to proxy to the gradle build stages (through System Properties)
```

## set up environment file

> `jenkins.env` **should be git ignored - has sensitive credentials**

```sh
DB_HOST=postgres
DB_PORT=5432
DB_NAME=test
DB_USER=test
DB_PASS=test
APP_PORT=8080

AWS_DEFAULT_REGION=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=

# student bucket name: lc-nga-c7-name-artifacts
ARTIFACTS_BUCKET_NAME=
# (NO leading or trailing slash): todo-api
ARTIFACTS_PROJECT_PREFIX=
```

> the AWS credentials should come from:

- convenient: `~/.aws/{config,credentials}`
- secure: create jenkins IAM user and copy the credentials over

# usage

## commands

> startup

```sh
docker-compose up -d

# optional override port
JENKINS_PORT=8008 docker-compose up -d
```

> access jenkins web GUI (change port if overridden)

- [http://localhost:8080](http://localhost:8080)

> get initial jenkins password

```sh
docker-compose exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

> view test reports to debug

- assumes your primary project is titled `Todo API compile` (change the path below if it is not)

```sh
docker run -it --rm -p 4000:4000 -v "docker_jenkins-data:/jenkins" python python -m http.server -d '/jenkins/workspace/Todo API compile/build/reports/tests/test' 4000
```

## export config

> used to export the jenkins config if you would like to host it without reconfiguring

- **note**: exported files will be owned by root (see comment below)
- **note**: if you dont clear the workspace before exporting it will be a big file (all the gradle/build cache will be included)
- override with compose vars
  - `EXPORT_VOLUME_NAME`: default `docker_jenkins-data`
  - `OUTPUT_DIR` default `exported[.tar.gz]`

> copy to `./exported` and zip to `./exported.tar.gz`

```sh
docker-compose -f docker-compose.export.yml up -d
```

> copy only

```sh
docker-compose -f docker-compose.export-jenkins.yml run -d copy
```

> zip only

```sh
docker-compose -f docker-compose.export-jenkins.yml run -d zip
```

> change owner to host user instead of root if needed

```sh
sudo chown -R <username>:<group> exported/ exported.tar.gz
```
